package model;

import ("github.com/jinzhu/gorm"
		"text/template"
	"bytes"
	"fmt"
)

const table_prefix = "cmslet_"

type Renderable interface {
	Render(map[string]interface{}) (output string, err error)
}

type Page struct {
	gorm.Model
	Url string
	Name string
	Description string
	MasterTemplate *Template
	PageTemplate   *Template

	sections []*Page `gorm:"ForeignKey:PageId"`

}

func (*Page) TableName() string {
	return table_prefix+"page"
}

/**
  Renders the latest versions of page templates.
 */
func (p *Page) Render (data map[string]interface{}) (output string, err error){
	var content string

    if content, err = p.PageTemplate.Render(data); err != nil {
    	return
	}

    data["CONTENT"] = content


    if output, err = p.MasterTemplate.Render(data); err != nil {
    	return
	}

	return
}



type Section struct {
	gorm.Model
	PageId  uint
	Page 	*Page
	Name string
	Text string  `json:'-' gorm:"type:text"`

}

func (*Section) TableName() string {
	return table_prefix+"section"
}

type Template struct {
	gorm.Model
	ProductionVersion *TemplateVersion
	DevelopmentVersion *TemplateVersion
	CurrentVersion *TemplateVersion
	Name string

}

/**
   Renders the current the 'production' ready template
 */
func (t *Template) Render (data map[string]interface{}) (output string, err error) {
	return t.ProductionVersion.Render(data)
}

type TemplateVersion struct {
	gorm.Model
	Filename string
	Template *template.Template

}


func (t *TemplateVersion) Render (data map[string]interface{}) (output string, err error) {

	var buf bytes.Buffer

	if t != nil && t.Template != nil {
		err = t.Template.Execute(&buf, data)
		if err != nil {
			err = fmt.Errorf("Error whilst generating email '%s'", err)
			return
		}
		output = buf.String()
	}

	return
}
