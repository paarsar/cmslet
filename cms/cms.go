package cms

import (
	m "cmslet/model"
	"path/filepath"
	"os"
	"log"
	"text/template"
	"fmt"
)

type CMS interface {
	CreatePage(name string, bodyTemplateUri string, masterTemplateUri string)
	RenderPage(name string, data map[string]interface{}) (output string, err error)
}

type cmsImpl struct {
	templateDir string
	templates map[string]*m.Template
	pages map[string]m.Renderable

}

func NewCMS(templateDir string) (cms CMS) {
	cms = &cmsImpl{templateDir: templateDir, pages:map[string]m.Renderable{}, templates:map[string]*m.Template{}}
	return
}

func (c *cmsImpl) newTemplate(uri string) (tmpl *m.Template) {

	tmpl = &m.Template{}
	tmpl.ProductionVersion = &m.TemplateVersion{}
	tmpl.ProductionVersion.Template = c.GetTemplate(uri)

	return
}


func (c *cmsImpl) CreatePage(name string, bodyTemplateUri string, masterTemplateUri string) {

	page := &m.Page{}

	page.PageTemplate = c.newTemplate(bodyTemplateUri)
	page.MasterTemplate = c.newTemplate(masterTemplateUri)

	c.pages[name] = page
	return
}

func (c *cmsImpl) RenderPage(name string, data map[string]interface{}) (output string, err error) {

	if page, ok := c.pages[name]; ok {
		return page.Render(data)
	} else {
		err = fmt.Errorf("The page '%s' does not exist ", name)
	}

	return
}

func (c *cmsImpl) GetTemplate(templateName string) (tmplate *template.Template) {

	var err error
	var root string

	root, err = filepath.Abs(filepath.Dir(os.Args[0]))
	templatePath := filepath.Join(root, c.templateDir)

	if err != nil {
		log.Fatal(err)
	}


	if tmplate, err =  template.ParseFiles(templatePath+"/"+templateName); err != nil {
		log.Printf("error ::: %+v", err)
		return
	}

	return
}


/*
func (c *cmsImpl) Render(data map[string]interface{}, tmplate *m.Template) (output string, err error) {

	var buf bytes.Buffer

	if tmplate!= nil {
		err = tmplate.ProductionVersion.Template.Execute(&buf, data)
		if err != nil {
			err = fmt.Errorf("Error whilst generating email '%s'", err)
			return
		}
		output = buf.String()
	}

	return
}

func (c *cmsImpl) GetTemplate(templateName string) (templateObj *m.Template) {

	var err error
	var root string

	root, err = filepath.Abs(filepath.Dir(os.Args[0]))
	templatePath := filepath.Join(root, c.templateDir)

	if err != nil {
		log.Fatal(err)
	}

	var ok bool
	if tmpl, ok = c.templates[templateName]; !ok {
		if tmpl, err =  template.ParseFiles(templatePath+"/"+templateName); err != nil {
			log.Printf("error ::: %+v", err)
			return
		}

		templateObj = &m.Template{}
		templateObj.ProductionVersion = &m.TemplateVersion{}
		templateObj.ProductionVersion.Template= tmpl

		//template.ProductionVersion = &m.TemplateVersion{}
		//template.ProductionVersion.Template

		c.templates[templateName] = tmpl

	} else {
		tmpl = c.templates[templateName]
	}
	return
}
*/




